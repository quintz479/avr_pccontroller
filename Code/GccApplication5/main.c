/*
* GccApplication4.cpp
*
* Created: 03.05.2017 19:56:14
* Author : Юра
*/

#define F_CPU 8000000L
#define XTAL F_CPU
//#define baudrate 225000L
#define baudrate 31250L
//#define bauddivider (XTAL/(16*baudrate)-1)
#define bauddivider 15
#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)

#define CHR_ESC				0x1B
#define CHR_BS				0x08
#define CHR_CR				0x0D
#define CHR_LF				0x0A


#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/interrupt.h>

#include <stdbool.h>
#include <avr/cpufunc.h>
#include <string.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "fifo.h"

#ifndef SREG
#  if __AVR_ARCH__ >= 100
#    define SREG _SFR_MEM8(0x3F)
#  else
#    define SREG _SFR_IO8(0x3F)
#  endif
#endif


//Global variables here

#define RX_BUFFER_SIZE 35
char rx_buffer[RX_BUFFER_SIZE];
unsigned char rx_wr_index,rx_rd_index,rx_counter;
char rx_buffer_overflow;
char read_enable = 0;
char process_data = 0;
volatile unsigned char trigger=1;
volatile unsigned char direction_pwm_led=1;
volatile unsigned char direction_bam=1;
volatile unsigned char pwm_led_counter=0;
volatile uint16_t vizualized_pwm0,vizualized_pwm1;
volatile uint16_t sleep_cur=0;


volatile unsigned char maxtest=9;


#define PWM_LED_NUM 1

#define N_PWM 8
volatile uint16_t pwm[N_PWM], direct[N_PWM];
unsigned char mask_b, mask_c, mask_d;
unsigned char stage=0;
uint16_t EEMEM pwm_mem[12]={0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF,0x3FF};
#define N_PARAMS 9
volatile uint16_t params[N_PARAMS]= {   31,   190,140,           0,        160,      300,       0,               0, 6};
uint16_t EEMEM params_mem[N_PARAMS]={   31,   190,140,           0,        160,      300,       0,               0, 6};
//flag,	speed, low border ,high border,	off time, on time, divider pwm led, shifttest
//0   , 1    , 2          , 3         , 4       , 5      , 6              , 7
FIFO(128 ) uart_tx_fifo;
//FIFO( 32 ) uart_rx_fifo;

#define SIMPLE_UART

void EraseEEPROM()
{
	uint8_t sreg, i;
	uint16_t addr;
	uint8_t clear[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	uint8_t data[8];

	sreg = SREG;
	cli();

	// Write page by page using the block writing method
	for(addr = 0; addr < 0x200; addr += 8)
	{
		eeprom_read_block((void *)&data[0], (const void *)addr, 8);
		for(i = 0; i < 8; i++)
		if(data[i] != 0xFF)
		{
			eeprom_write_block((void*)&clear[0], (void*)addr, 8);
			break;
		}
	}
	
	SREG = sreg;
}

void TimerLEDInit(void)
{

	// set up timer with prescaler = 256
	TCCR0 |= (1 << CS02)| (0 << CS01)| (1 << CS00);
	
	// initialize counter
	TCNT0 = 0;
	
	// enable overflow interrupt
	TIMSK |= (1 << TOIE0);
}

void TimerInit(void)
{

	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 8000,000 kHz
	// Mode: CTC top=OCR1A
	// OC1A output: Discon.
	// OC1B output: Discon.
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer 1 Overflow Interrupt: On
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: Off
	// Compare B Match Interrupt: Off
	TCCR1A = (0<<COM1A1)|(0<<COM1A0)|(0<<COM1B1)|(0<<COM1B0)|(0<<FOC1A)|(0<<FOC1B)|(0<<WGM11)|(0<<WGM10);
	TCCR1B = (0<<ICNC1) |(0<<ICES1) |(0)        |(0<<WGM13) |(1<<WGM12)|(0<<CS12) |(1<<CS11) |(0<<CS10);
	TCNT1H = 0x00;
	TCNT1L = 0x00;
	ICR1H  = 0x00;
	ICR1L  = 0x00;
	OCR1AH = 0x00;
	//OCR1AL = 0x10;
	OCR1AL = 0x00;
	OCR1BH = 0x00;
	OCR1BL = 0x00;
	TIMSK  = 0x10;
	
}
void NewLEDTimerInit(void)
{

	//TIMSK |= (1<<OCIE2);  // timer2: compare interrup is enable
	TCCR2 = (1<<WGM21)|(1<<WGM20)|(1<<COM21)|(0<<COM20);  // FPWM
	TCCR2|= (0<<CS22) |(1<<CS21)| (0<<CS20); // prescaler 1/1024
	//OCR2  = 0x40;
	
}

void USARTInit(void)
{
	UBRRH = HI(bauddivider);
	UBRRL = LO(bauddivider);
	
	UCSRB = 1<<RXEN|1<<TXEN|1<<RXCIE;//|1<<TXCIE;
	//ucsrb recieveENABPWM transmitENABLE
	//recieveINTERenable transmitINTERdisable
	#ifdef SIMPLE_UART
	UCSRC = 1<<URSEL|1<<UCSZ0|1<<UCSZ1; //8bit
	#else
	UCSRC=(1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1)|(1<<USBS)|(1<<UPM1);
	#endif
	//		must		8bit		8bit	2stop		even
}

void uart_send( char data)
{
	FIFO_PUSH(uart_tx_fifo, data);
}
void uart_send_( char data)
{
	while(!( UCSRA & (1 << UDRE)));   // Ожидаем когда очистится буфер передачи
	UDR = data; // Помещаем данные в буфер, начинаем передачу
}
void uart_send_str( char *s)
{
	while (*s != 0) uart_send(*s++);
}

uint16_t uint16ifmem(uint16_t a,uint16_t max,uint16_t thenn ){if (a==max) {return thenn;}else{return a;}}
char chrifmem(char a,char max,char thenn ){if (a==max) {return thenn;}else{return a;}}
unsigned char uchrifmem(unsigned char a,unsigned char max,unsigned char thenn ){if (a==max) {return thenn;}else{return a;}}

void uart_send_str_(void)
{
	while (FIFO_COUNT(uart_tx_fifo) != 0)
	{
		uart_send_(FIFO_FRONT(uart_tx_fifo));
		FIFO_POP(uart_tx_fifo);
	}
}
void uart_clear_screen(void)
{
	uart_send(CHR_ESC);
	uart_send_str("[2J");
	uart_send(CHR_ESC);
	uart_send_str("[H");
}
void uart_clear_line(void)
{
	uart_send(CHR_ESC);
	uart_send_str("[2K");
	uart_send(CHR_ESC);
	uart_send_str("[99D");

}
void uart_new_line(void)
{
	//uart_send(CHR_CR);
	//uart_send(CHR_LF);
}

uint16_t strtoint(const char * str,unsigned char start,unsigned char count)
{
	char valuePWM[count];
	memcpy(valuePWM,&str[start],count);
	return atoi(valuePWM);
}

//ISR(TIMER2_COMP_vect)
//{
// keep a track of number of overflows
//uart_send_str("8");
//	PORTB = PORTB ^ (1<<0);
//}


ISR(TIMER0_OVF_vect)
{
	cli();
	if (sleep_cur>0){sleep_cur--;TCNT0=0xff-2;}else
	{
		if (direction_pwm_led)TCNT0=params[2];else TCNT0=params[1];
		trigger++;
		if (trigger>=params[7]){
			trigger=0;
			if (direction_pwm_led) pwm_led_counter++; else pwm_led_counter--;
			if (pwm_led_counter>(0xff-1)) {direction_pwm_led=0;sleep_cur=params[6];}
			else if (pwm_led_counter<1) {direction_pwm_led=1;sleep_cur=params[5];pwm_led_counter=0;}
		}
	}
	sei();
}

ISR(TIMER1_COMPA_vect)
{
	cli();
	TCNT1 = 0;      //Обнуляем таймер-счетчик, иначе он будет продолжать считать до 0xFFFF
	OCR1A=1<<stage<<params[8];
	//Паузу делаем после текущего бита. Т.е. если текущий - 7, пауза максимальная]

	PORTB = mask_b;//|((PINB & (1<<PINB0)));
	PORTC = mask_c;
	//Следующее значение stage
	
	if (stage<(maxtest)) stage++;
	else {stage=6;}


	mask_b = 0;
	mask_c = 0;
	mask_d = 0;
	
	
	
	//if( (vizualized_pwm0 & (1 << stage)) != 0)
	//mask_b |= (1 << 0);
	//if( (vizualized_pwm1 & (1 << stage)) != 0)
	//mask_b |= (1 << 1);
	if( (pwm[0] & (1 << stage)) != 0)
	mask_b |= (1 << 0);
	if( (pwm[1] & (1 << stage)) != 0)
	mask_b |= (1 << 1);
	if( (pwm[2] & (1 << stage)) != 0)
	mask_b |= (1 << 4);
	if( (pwm[3] & (1 << stage)) != 0)
	mask_b |= (1 << 2);
	if( (pwm[4] & (1 << stage)) != 0)
	mask_c |= (1 << 2);
	if( (pwm[5] & (1 << stage)) != 0)
	mask_c |= (1 << 1);
	if( (pwm[6] & (1 << stage)) != 0)
	mask_c |= (1 << 0);
	if( (pwm[7] & (1 << stage)) != 0)
	mask_b |= (1 << 5);
	//if( (pwm[8] & (1 << stage)) != 0)
	//mask_c |= (1 << 2);
	//if( (pwm[9] & (1 << stage)) != 0)
	//mask_c |= (1 << 3);
	//if( (pwm[10] & (1 << stage)) != 0)
	//mask_c |= (1 << 4);
	//if( (pwm[11] & (1 << stage)) != 0)
	//mask_c |= (1 << 5);



	sei();
}

ISR(USART_RXC_vect)
{
	cli();
	char data=UDR;
	
	if ((data == 'x') || (data == 'X'))
	{
		memset(&rx_buffer[0], 0, sizeof(rx_buffer));
		rx_wr_index = 0;
		read_enable = 0;
		//uart_clear_line();
	}
	else{
		if (read_enable == 0)
		{
			memset(&rx_buffer[0], 0, sizeof(rx_buffer));
			rx_wr_index = 0;
			read_enable = 1;
		}
		if(((data == 13)||((data == 10)))&&(read_enable == 1))
		{
			uart_send('.');
			uart_new_line();
			//uart_send('>');
			read_enable = 0;
			process_data = 1;
		}
		
		if (read_enable == 1)
		{
			uart_send(data);
			rx_buffer[rx_wr_index++]=data;
			if (rx_wr_index == RX_BUFFER_SIZE)
			rx_wr_index=0;
			if (++rx_counter == RX_BUFFER_SIZE)
			{
				rx_counter=0;
				rx_buffer_overflow=1;
			}
		}
	}
	sei();
}


int main(void)
{
	DDRB = 0xFF;
	DDRC = 0xFF;
	DDRD = 0xFF;
	cli();
	TimerInit();
	TimerLEDInit();
	NewLEDTimerInit();
	USARTInit();

	
	
	for(unsigned char i = 0; i < N_PWM; i++)
	{
		pwm[i] = uint16ifmem(eeprom_read_word(&pwm_mem[i]),0xffff,30/100.0*1023);
		direct[i] = 0;
	}

	for(unsigned char i = 0; i < 8; i++)
	{
		uint16_t read = eeprom_read_word(&params_mem[i]);
		if (read!=0xffff)
		params[i] = read;
	}
	

	sei();

	while(1)
	{	unsigned char aaaa=0;
		if (params[0]==30)
		aaaa=params[3]+sin((1.57/255.0)*pwm_led_counter)*(params[4]-params[3]);//mid sin to top sin
		else if (params[0]==31)
		aaaa=params[3]+((sin(4.7124+pwm_led_counter/81.1693)+1.0)/2.0)*((params[4]-params[3])*1.0);//sin
		else if (params[0]==32)
		aaaa=params[3]+pwm_led_counter/255.000*(params[4]-params[3]); //line
		else if (params[0]==33)
		aaaa=pwm_led_counter;
		else
		aaaa=params[0];
		OCR2=aaaa;

		if (FIFO_COUNT(uart_tx_fifo)!=0) uart_send_str_();

		if (process_data==1)
		{
			process_data=0;
			if (!strncmp(rx_buffer,"pw",2))	//pwa 100 //
			{								//01234567
				pwm[rx_buffer[2]-'a']=strtoint(rx_buffer,4,3)/100.0*1023;
				eeprom_write_word(&pwm_mem[rx_buffer[2]-'a'], pwm[rx_buffer[2]-'a']);
			}
			else if (!strncmp(rx_buffer,"nm",2))	//xnmffffff //50%
			//xnmkkkkkk //100%
			// 01234567
			//a=0,b=1,c=2,d=3,e=4,f=5
			//g=6,h=7,i=8,j=9,k=10
			{
				for (unsigned char i=0;i<N_PWM;i++)
				{
					char a=rx_buffer[i+2]-'a';
					if ((a>=0)&&(a<=10))
					{
						pwm[i]=a/10.0*1023;
						eeprom_write_word(&pwm_mem[i], pwm[i]);
					}
				}
			}
			
			else if (!strncmp(rx_buffer,"mm",2))
			//xmm0311900001600030000000006
			//xmm------------------------6
			//xmm031
			//volatile uint16_t params[N_PARAMS]= {   31,   190,140,           0,        160,      300,       0,               0, 6};
			//xmm  031   190      140        000         160          00300     00000    00               6        //29+3
			// 01  234   567      890        123         456          78901     23456    78               9
			//     flag, speeddown, speedup, low border, high border, off time, on time, divider pwm led, shifttest
			
			{
				unsigned char curptr=2, len[]={3,3,3,3,3,5,5,2,1};
				for (unsigned char i=0;i<N_PARAMS;i++)
				{
					if (rx_buffer[curptr] >= '0' && rx_buffer[curptr] <= '9'){
						uint16_t a=strtoint(rx_buffer,curptr,len[i]);
						if ((a>=0)&&(a<=0xFFFF))
						{
							params[i]=a;
							eeprom_write_word(&params_mem[i], params[i]);
						}
					}
					curptr+=len[i];
				}
			}
			else if (!strncmp(rx_buffer,"pz",2))	//pz 100 //
			{								//012345
				for(unsigned char i = 0; i < N_PWM; i++)
				{
					pwm[i] = strtoint(rx_buffer,i+2,3)/100.0*1023;
					eeprom_write_word(&pwm_mem[i], pwm[i]);
				}
			}
			else if (!strncmp(rx_buffer,"sp",2))	//sp 190 //speed of changing led pwm value
			{										//012345
				params[2]=strtoint(rx_buffer,3,3);
				eeprom_write_word(&params_mem[2], params[2]);
			}
			else if (!strncmp(rx_buffer,"di",2))	//di 3 //divider
			{										//012345
				params[7]=strtoint(rx_buffer,3,3);
				eeprom_write_word(&params_mem[7], params[7]);
			}
			else if (!strncmp(rx_buffer,"lo",2))	//lo 200 //low border led change
			{										//012345
				params[3]=strtoint(rx_buffer,3,4);
				eeprom_write_word(&params_mem[3], params[3]);
			}
			else if (!strncmp(rx_buffer,"hi",2))	//hi 1023 //high border led change //hi 600
			{										//0123456
				params[4]=strtoint(rx_buffer,3,4);
				eeprom_write_word(&params_mem[4], params[4]);
			}
			else if (!strncmp(rx_buffer,"sll",3))	//sll 0 //time to sleep low state change - led //sl 300
			{										//01234567
				params[5]=strtoint(rx_buffer,4,5);
				eeprom_write_word(&params_mem[5], params[5]);
			}
			else if (!strncmp(rx_buffer,"slh",3))	//slh 0 //time to sleep high state change - led //sl 300
			{										//01234567
				params[6]=strtoint(rx_buffer,4,5);
				eeprom_write_word(&params_mem[6], params[6]);
			}
			else if (!strncmp(rx_buffer,"fu",2))	//fu 1020 //function change
			{										//0123456
				params[0]=strtoint(rx_buffer,3,4);
				eeprom_write_word(&params_mem[0], params[0]);
			}
			else if (!strncmp(rx_buffer,"dd",2))	//dd 3 //shift BAM bit
			{										//0123
				unsigned char tmp=rx_buffer[3]-'0';
				if ((tmp<10)&&(tmp>0)){
					params[8]=tmp;
				eeprom_write_word(&params_mem[8], params[8]);}
			}
			//else if (!strncmp(rx_buffer,"fa",2))	//fa0 1020 //fan speed change
			//{										//01234567
			//	pwm[strtoint(rx_buffer,2,1)]=strtoint(rx_buffer,4,4);
			//}
			else if (!strncmp(rx_buffer,"cl",2)) uart_clear_screen();
			else if (!strncmp(rx_buffer,"rr",2)) EraseEEPROM();
			else if (!strncmp(rx_buffer,"ab",2))
			{
				uart_send_str("abcdefghijkl");
				uart_new_line();
			}
			else if (!strncmp(rx_buffer,"in",2))
			{
				char str1[100];
				sprintf(str1, "sp:%d div:%d low:%u hi:%u slp:%u sft:%u bit:%u",params[2],params[7],params[3],params[4],params[5],params[8],maxtest);
				uart_send_str(str1);
				uart_new_line();
			}
			else if (!strncmp(rx_buffer,"ww",2))	//wwa
			{										//012
				char str1[6];
				for(unsigned char i = 0; i < N_PWM; i++)
				{
					sprintf(str1, "%u %u",i,pwm[i]);
					uart_send_str(str1);
					uart_new_line();
					uart_send_str_();
				}
				//sprintf(str1, "%u",pwm[rx_buffer[2]-'a']);
				//uart_send_str(str1);
				//uart_new_line();
			}
			// 			else if (!strncmp(rx_buffer,"ee",2))
			// 			{
			// 				char str1[10];
			// 				sprintf(str1, "%d",strtoint("uu 1234",3,3));
			// 				uart_send_str(str1);
			// 				uart_new_line();
			// 			}


			else
			{
				uart_send_str("Unkn cmd");
				//uart_send_str(rx_buffer);
				uart_new_line();
				
			}

		}
	}
}